'''
Created on 20 oct. 2020

@author: RSSpe
'''

from nodo import Nodo


class ListaEnlazada():
    "" "Clase para crear y gestionar nuestra lista enlazada" ""

    def __init__(self):
        self.__centinelaInicio = Nodo()
        self.__centinelaFinal = Nodo()
        self.__centinelaInicio.siguiente = self.__centinelaFinal
        self.__cantidadElementos = 0


    @property
    def cantidadElementos(self):
        return self.__cantidadElementos


    def __centinelaInicioTomaA(self, nuevo):
        "" "Metodo para tomar otro valor en el centinela" ""
        self.__centinelaInicio.siguiente = nuevo


    def vaciarLista(self):
        "" "Metodo para vaciar la lista" ""
        self.__centinelaInicio.siguiente = None
        self.__centinelaFinal.siguiente = None
        self.__cantidadElementos = 0


    def __verificarNoVacia(self):
        "" "Metodo para verificar si la lista esta vacia" ""
        return self.__cantidadElementos!=0


    def mostrarCantidadElementos(self):
        "" "Metodo para mostrar la cantidad de nodos agregados" ""
        print(self.__cantidadElementos)
    
    
    def conectarEntre(self, conector, receptor, conector2):
        "" "Metodo para unir a los nodos" ""
        conector.siguiente = receptor
        receptor.siguiente = conector2
        self.__cantidadElementos+=1


    def agregarElementoInicio(self, dato):
        "" "Metodo para agregar un elemento al inicio de la lista" ""
        self.conectarEntre(self.__centinelaInicio, Nodo(dato), self.__centinelaInicio.siguiente)

        return True


    def agregarElementoFinal(self, dato):
        "" "Metodo para agregar un elemento al final de la lista" ""
        tmp = self.__centinelaInicio

        while tmp.siguiente!=self.__centinelaFinal:
            tmp = tmp.siguiente

        self.conectarEntre(tmp, Nodo(dato), tmp.siguiente)

        return True


    def verLista(self):
        "" "Metodo para ver la lista" ""
        if self.__verificarNoVacia():
            tmp = self.__centinelaInicio.siguiente

            print("\nLista: ")
            while tmp!=self.__centinelaFinal:
                print(f"[{tmp.dato}]--> ", end="")
                tmp = tmp.siguiente

        else:
            print("La lista esta vacia")


    def eliminarElementoInicio(self):
        "" "Metodo para eliminar un elemento en el inicio" ""
        if self.__verificarNoVacia():
            elemento = self.__centinelaInicio.siguiente
        
            self.__centinelaInicio.siguiente = self.__centinelaInicio.siguiente.siguiente
            self.__cantidadElementos-=1

            return elemento
        else:
            return None


    def eliminarElementoFinal(self):
        "" "Metodo para eliminar un elemento en el final" ""
        if self.__verificarNoVacia():
            elemento = self.__centinelaInicio
            tmp = None

            while elemento.siguiente!=self.__centinelaFinal:
                tmp = elemento
                elemento = tmp.siguiente

            tmp.siguiente = self.__centinelaFinal
            self.__cantidadElementos-=1
        
            return elemento

        else:
            return None


    def mostrarUltimoElemento(self):
        "" "Metodo para mostrar el ultimo elemento" ""
        if self.__verificarNoVacia():
            tmp = self.__centinelaInicio.siguiente

            while tmp.siguiente!=self.__centinelaFinal:
                tmp = tmp.siguiente

            print(tmp)
        else:
            print("No hay elementos")


    def mostrarPrimerElemento(self):
        "" "Metodo para mostrar el primer elemento" ""
        if self.__verificarNoVacia():
            print(self.__centinelaInicio.siguiente)
        else:
            print("No hay elementos")


    def buscarElemento(self, dato):
        "" "Metodo para buscar un elemento" ""
        if self.__verificarNoVacia():
            tmp = self.__centinelaInicio
            encontrado = False

            while(tmp!=self.__centinelaFinal):
                tmp = tmp.siguiente
                if tmp.dato==dato:
                    encontrado = True
                    break
            
            if encontrado:
                print("Encontrado")
            else:
                print("No se ha encontrado")

        else:
            print("No hay elementos")


    def agregarElementoEnPosicion(self, dato, pos):
        "" "Metodo para agregar un elemento en una posicion" ""
        if pos>=0 and pos<=self.__cantidadElementos:

            cont = 0
            tmp = self.__centinelaInicio

            if self.__cantidadElementos==cont:
                while cont!=pos:
                    tmp = tmp.siguiente
                    cont+=1

                self.conectarEntre(tmp, Nodo(dato), tmp.siguiente)
            else:
                while cont!=pos:
                    tmp = tmp.siguiente
                    cont+=1
    
                self.conectarEntre(tmp, Nodo(dato), tmp.siguiente)

            return True

        else:
            return False


    def eliminarElementoEspecifico(self, dato):
        "" "Metodo para eliminar un elemento dado" ""
        if self.__verificarNoVacia():
            eliminado = None

            if dato==self.__centinelaInicio.siguiente.dato:
                eliminado = self.__centinelaInicio.siguiente
                self.__centinelaInicioTomaA(self.__centinelaInicio.siguiente.siguiente)
                self.__cantidadElementos-=1
                return eliminado

            else:
                anterior = None
                sucesor = self.__centinelaInicio.siguiente
                
                while sucesor!=self.__centinelaFinal:
                    if dato==sucesor.dato:
                        eliminado = sucesor
                        anterior.siguiente = sucesor.siguiente
                        self.__cantidadElementos-=1
                        return eliminado

                    anterior = sucesor
                    sucesor = sucesor.siguiente
            
            print("\nNo existe ese elemento")

        else:
            return None

    '''
    def eliminarElementoEnPosicion(self, pos):
        "" "Metodo para eliminar un elemento en una posicion" ""
        if not(pos>=0 and pos<self.__cantidadElementos) or not self.__verificarNoVacia():
            return None

        else:
            tmp = self.__centinelaInicio.siguiente
            eliminar = None
            
            if pos==0:
                eliminar = tmp
                self.__centinelaInicioTomaA(eliminar.siguiente)

            elif pos==self.__cantidadElementos-1:

                while tmp.siguiente.siguiente!=self.__centinelaFinal:
                    tmp = tmp.siguiente

                eliminar = tmp.siguiente
                tmp.siguiente = self.__centinelaFinal


            else:
                cont=0

                while cont!=pos-1:
                    tmp = tmp.siguiente
                    cont+=1

                eliminar = tmp.siguiente
                tmp.siguiente = eliminar.siguiente

            self.__cantidadElementos-=1

            return eliminar
    '''
