'''
Created on 20 oct. 2020

@author: RSSpe
'''

from lista_enlazada import ListaEnlazada


def ingresarSubOpcion(sub):
    while(True):
        print("---------------MENU---------------")
        print("¿Donde lo quieres realizar?")
        print("1.- Inicio")
        print("2.- Final")
        if sub==0:
            print("3.- Posicion especifica")
        else:
            print("3.- Elemento especifico")

        opcion = int(input("Introduce una opcion: "))
        
        if opcion>0 and opcion<4:
            return opcion
        else:
            print("Opcion invalida, prueba de nuevo")
        
        print()


def ingresarDato():
    while(True):
        opcion = int(input("Introduce un dato: "))
        
        if opcion>-1:
            return opcion
        else:
            print("El dato no puede ser negativo")
        
        print()


def operacion(limite):
    while(True):
        opcion = int(input("Introduce una posicion: "))
        
        if opcion>-1 and opcion<limite:
            return opcion
        else:
            print("Posicion incorrecta, por favor vuelve a intentarlo")
        
        print()


def ingresarPosicionAgregar(limite):
    if limite==1:
        return 0
    else:
        return operacion(limite)


def ingresarPosicionEliminar(limite):
    if limite==0:
        return 0
    else:
        return operacion(limite)


miLista = ListaEnlazada()
candado = True

while candado:

    try:
        while(True):
            print("---------------MENU---------------")
            print("1.- Agregar elemento")
            print("2.- Eliminar elemento")
            print("3.- Recorrer elementos")
            print("4.- Buscar elemento")
            print("5.- Vaciar lista")
            print("6.- Mostrar cantidad de elementos")
            print("7.- Mostrar primer elemento")
            print("8.- Mostrar ultimo elemento")
            print("9.- Salir")

            opcion = int(input("Introduce una opcion: "))

            if opcion>0 and opcion<10:
                break
            else:
                print("Opcion invalida, prueba de nuevo")
        
            print()


        if opcion==1:
            subOpcion = ingresarSubOpcion(0)

            if subOpcion==1:
                print(miLista.agregarElementoInicio(ingresarDato()))
            elif subOpcion==2:
                print(miLista.agregarElementoFinal(ingresarDato()))
            else:
                print(miLista.agregarElementoEnPosicion(ingresarDato(), ingresarPosicionAgregar(miLista.cantidadElementos+1)))


        elif opcion==2:
            subOpcion = ingresarSubOpcion(1)

            if subOpcion==1:
                print(miLista.eliminarElementoInicio())
            elif subOpcion==2:
                print(miLista.eliminarElementoFinal())
            else:
                if miLista.cantidadElementos!=0:
                    print(miLista.eliminarElementoEspecifico(ingresarDato()))
                else:
                    print("No puedes eliminar porque no hay elementos")


        elif opcion==3:
            miLista.verLista()

        elif opcion==4:
            miLista.buscarElemento(ingresarDato())
           
        elif opcion==5:
            miLista.vaciarLista()
            print("Lista vaciada")

        elif opcion==6:
            print("Elementos existentes: ", miLista.cantidadElementos)

        elif opcion==7:
            miLista.mostrarPrimerElemento()

        elif opcion==8:
            miLista.mostrarUltimoElemento()

        elif opcion==9:
            candado = False
        
    except ValueError as error:
        print("Error en la entrada de datos<", error,">, vuelve a intentarlo")

    print()

print("Fin    :)")
