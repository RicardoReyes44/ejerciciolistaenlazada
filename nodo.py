'''
Created on 20 oct. 2020

@author: RSSpe
'''

class Nodo():
    "" "Clase para crear nodo" ""

    def __init__(self, dato=None):
        self.__dato = dato
        self.__siguiente = None

    @property
    def dato(self):
        return self.__dato

    @property
    def siguiente(self):
        return self.__siguiente

    @siguiente.setter
    def siguiente(self, siguiente):
        self.__siguiente = siguiente
    
    def __str__(self):
        return f"Nodo = dato({self.__dato})"
